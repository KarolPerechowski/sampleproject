﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectOne.Models.Repositories
{
    public class PersonRepository : Repository<Person>
    {
        public List<Person> getByName(String name)
        {
            return DbSet.Where(a => a.FirstName.Contains(name)).ToList();
        }
    }
}