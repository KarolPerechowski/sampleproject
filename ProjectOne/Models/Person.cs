﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace ProjectOne.Models
{
    public class Person
    {
        public int PersonID { get; set; }

        [DisplayName("Imię")]
        [Required(ErrorMessage = "Musisz wpisać imię.")]
        [StringLength(15, ErrorMessage = "Imię nie może być dłuższe niż 15 znaków.")]
        public string FirstName { get; set; }

        [DisplayName("Nazwisko")]
        public string LastName { get; set; }

        [DisplayName("Płeć")]
        public string Gender { get; set; }

        [DisplayName("Wiek")]
        public int Age { get; set; }
    }
}