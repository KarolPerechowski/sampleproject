﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ProjectOne.Models
{
    public class PersonDataContext : DbContext
    {
        public DbSet<Person> PersonList { get; set; }
    }
}