﻿using ProjectOne.Models;
using ProjectOne.Models.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectOne.Controllers
{
    public class PersonController : Controller
    {
        //PersonDataContext context = new PersonDataContext();
        PersonRepository repo = new PersonRepository();
        // GET: Person
        public ActionResult Index()
        {
            return View(repo.GetAll());
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Person person)
        {
            if (!ModelState.IsValid) return View(person);
            repo.Add(person);
            repo.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}